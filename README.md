# PwdGen

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

<h1>Aqua Scanning in Gitlab CI</h1>

## Introduction

The purpose of this project is to demonstrate Aqua scanning as part of the CI/CD pipeline.
We will walk through a short use case with a simple, containerised Angular application,
with an existing build job. We will add in a stage to validate the image with Aqua against our
defined Image Assurance Policies.

## Steps

1. Fork this repository to your own user
2. Within your Aqua instance
    - [ ] Navigate to Administration -> Access Management
    - [ ] Add a new user to perform the scanning activities
        - [ ] Name: `scanner`
        - [ ] Set and make note of a Password
        - [ ] Role: Scanner
    - [ ] Navigate to Administration -> Integrations -> Image Registries
        - [ ] Add registry
            - [ ] Name: `Gitlab` (we'll need to match this later so make a note if you choose a different name)
            - [ ] Registry Type: `Docker Registry (v1/v2)`
            - [ ] Registry URL: `https://registry.gitlab.com`
            - [ ] Username: Your Gitlab username
            - [ ] Password: Your Gitlab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (scoped with `read_api` and `read_registry`)
                - Make one if you don't have one already, instructions in the link above.
                - NOTE: Validate the connection using the test connection button, if the first 2 tests pass, that's fine. Save and continue.
                - The third test (image search) may fail, but that won't prevent Aqua performing scans on individual images. This is due to the registry type.
3. Within your repository, Navigate to Settings (bottom left) -> CI/CD -> Variables
    - [ ] We're going to create a few variables to allow us to make use of our credentials without needing to commit them to our `.gitlab-ci.yml` file.
    - [ ] AQUA_USER - The name of your scanner user (`scanner`)
    - [ ] AQUA_PASSWORD - The password for your scanner user
    - [ ] AQUA_SERVER - The base URL of your Aqua instance
    - [ ] DOCKER_AUTH_CONFIG
        - This is used to authenticate with the Aqua image registry to pull the scanner image.
        - You will need your Aqua registry credentials in a base64 encoded format:
            - Mac+Linux: `echo -n 'myuser@example.com:mypassword' | base64`
            - Windows PowerShell: `write-output $([Convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes("myuser@example.com:mypassword")))`
        - Copy the base64 encoded credentials into the template below, in the DOCKER_AUTH_CONFIG variable value field
```json
{
    "auths": {
        "registry.aquasec.com": {
            "auth": "<base64 encoded credentials here>"
        }
    }
}
```
4. Edit the stages section at the top of the `.gitlab-ci.yml` file and add a new `scan` stage.
```yml
stages:
    - build
    - scan
```
5. Add a scan job to the `.gitlab-ci.yml`. This will perform the scan, using the scanner image from the Aqua container registry.
```yml
scan-job:
  image:
    name: registry.aquasec.com/scanner:6.0 # The scanner image from the Aqua registry. Ensure you use a compatible version for the instance
    entrypoint: [""] # Override the entrypoint of the image. By default, the scanner image will try to launch using the scannercli, but we want to call it from a shell in Gitlab
  stage: scan # Should match the name of the stage you added in step 4
  variables:
    IMAGE_TAG: $CI_PROJECT_PATH:$CI_COMMIT_SHORT_SHA # Gitlab built in variables to ensure image name uniqueness using git commit hash
  script:
    - /opt/aquasec/scannercli scan -H $AQUA_SERVER -U $AQUA_USER -P $AQUA_PASSWORD --registry "Gitlab" $IMAGE_TAG --show-negligible --register-compliant --htmlfile $CI_PROJECT_DIR/aqua-scan.html --jsonfile $CI_PROJECT_DIR/aqua-scan.json >/dev/null
# command syntax: /opt/aquasec/scannercli scan -H <Aqua host> -U <scan user> -P <scan user password> --registry <registry integration name> [image to scan] [options] --htmlfile <html report output location> --jsonfile <json report output location>
  artifacts:
    paths:
      - aqua-scan.json # Specify the report files to attach them to the job
      - aqua-scan.html
```
6. Save and commit your changes, a pipeline should be automatically triggered and viewable under CI/CD -> Pipelines on the left navigation menu from your repository.
7. In Aqua -> Images, you should have an image available, named like `youruser/simple-node-app:abc123de`
